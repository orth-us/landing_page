---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: splash
classes: landing
permalink: /
header:
  overlay_color: "#000"
  overlay_filter: "0.5"
  overlay_image: /assets/images/landing_wide.png
  actions:
    - label: "Get in touch!"
      url: "mailto:a.maia-chagas@sussex.org"
#excerpt: "ORTH is a space dedicated to the development of innovative open projects and solutions"

feature_row:
  - image_path: /assets/images/news.png
    title: "News"
    excerpt: "News about the hub"
    url: "/news/"
    btn_label: "Read More"
    btn_class: "btn--primary"

  - image_path: /assets/images/mission.png
    title: "Mission"
    excerpt: "Our 'whys' and 'hows'	"
    url: "/mission"
    btn_label: "Read More"
    btn_class: "btn--primary"

  - image_path: /assets/images/projects.png
    title: "Projects"
    excerpt: "Our past and current projects!"
    url: "/projects"
    btn_label: "Read More"
    btn_class: "btn--primary"

  - image_path: /assets/images/people.png
    title: "People"
    excerpt: "Meet the people behind our projects"
    url: "people"
    btn_label: "Read More"
    btn_class: "btn--primary"

  - image_path: /assets/images/partners.png
    title: "Partners"
    excerpt: "Meet our great partners"
    url: "partners"
    btn_label: "Read More"
    btn_class: "btn--primary"

#  - image_path: /assets/images/funding.png
#    title: "Funding"
#    excerpt: "Links to our funders and funding"
#    url: "funding"
#    btn_label: "Read More"
#    btn_class: "btn--primary"

  - image_path: /assets/images/funding.png
    title: "Resources"
    excerpt: "Lectures, tutorials and other materials"
    url: "resources"
    btn_label: "Read More"
    btn_class: "btn--primary"

---

### Welcome!

The Open Research Technologies Hub at the University of Sussex (ORTHUS) is a new
initiative to working on two main goals:

1-Train and foster a generation of open innovation experts who will learn open source/open science (OS) practices, principles of business, community building and project management to solve issues relevant to their communities, using context appropriate solutions. 

2- Create a network of hubs that will promote open practices and open development in academic spaces around the globe. 


Interested in knowing more?

- [Drop us a line](mailto:a.maia-chagas@sussex.ac.uk) or visit us during office hours (Mon-Wed 12:00 to 14:00 UK time), either in person (we are located in the central library, 2nd floor) or via [zoom](https://universityofsussex.zoom.us/j/97672053299)

--- 


{% include feature_row %}


---



