---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: splash
classes: landing
title: "Partners"
permalink: /partners/ 
header:
  overlay_color: "#000"
  overlay_filter: "0.5"
  overlay_image: /assets/images/landing_wide.png
  actions:
    - label: "Get in touch!"
      url: "mailto:a.maia-chagas@sussex.org"

feature_row:
  - image_path: /assets/images/trend_logo.png
    title: "Trend in Africa"
    url: "https://trendinafrica.org"
    btn_label: "Trend's homepage"
    btn_class: "btn--primary"

  - image_path: /assets/images/biortc_logo_small.png
    title: "BioRTC"
    url: "https://biortc.com"
    btn_label: "BioRTC's homepage"
    btn_class: "btn--primary"

  - image_path: /assets/images/biortc_logo_small.png
    title: "University of Sao Paulo"
    url: "/projects"
    btn_label: "Read More"
    btn_class: "btn--primary"

#- image_path: /assets/images/projects.png
#  title: "University of Sao Paulo"
#  url: "/projects"
#  btn_label: "Read More"
#  btn_class: "btn--primary"

---


{% include feature_row %}

