---
   layout: single
   author_profile: false
   title: "Mission"
   permalink: /mission/ 
---


ORTH-US is a central facility located at the library, with two main goals: 

 - train members of the Sussex community (academics, professional services, students and technicians), on skills and know-how needed to improve research outcomes and 

 - Host academics from partner institutions and train them in project based settings, to acquire the skills and background needed in leveraging open technologies to solve their local issues, while becoming a link in a network of connected hubs, able to support each other and exchange knowledge. 
 

